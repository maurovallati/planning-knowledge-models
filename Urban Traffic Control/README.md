# Planning for Urban Traffic Control

This set include PDDL+ models used for controlling traffic light phases in order to deal with congestion in urban areas and to deal with unexpected traffic conditions.

As we are continuously changing the models, here I provide pointers to the paper in which the overall idea, and core aspects of the model, are introduced and described.

**
Thomas Leo McCluskey, Mauro Vallati:
Embedding Automated Planning within Urban Traffic Management Operations. ICAPS 2017: 391-399

**
Mauro Vallati, Daniele Magazzeni, Bart De Schutter, Lukás Chrpa, Thomas Leo McCluskey:
Efficient Macroscopic Urban Traffic Models for Reducing Congestion: A PDDL+ Planning Approach. AAAI 2016: 3188-3194
