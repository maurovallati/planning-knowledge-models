(define (problem example)
  (:domain ChairGame)
  (:objects
  A B C D E F G H - person
  NO - empty)
  
    (:init
    (before A B)
    (before B F)
    (before F D)
    (before D E)
    (before E NO)
    (before NO G)
    (before G H)
    (before H C)
    (before C A)
    )
 (:goal
    (and
  (before A B)
  (before B C)
  (before C D)
  (before D E)
  (before E F)
  (before F G)
  (before G H)
    )
  )
)    
    