#! /usr/bin/env python

## ChairGame DOMAIN GENERATOR. 
## author: Mauro Vallati -- University of Huddersfield

import string
import sys
import random

### steps_taken defines how far you want to get from the initial configuration. it gives some indication on the complexity of finding a solution.

def help():
	print 'usage: generator.py <number_of_people> <number_empty_seats> <steps_taken>'
	sys.exit(2)






if len(sys.argv) != 4:
	help()
people=int(sys.argv[1])
empty=int(sys.argv[2])
steps=int(sys.argv[3])
if people < 4:
	print 'Not enough people. At least 4 are needed.'
	help()
if empty < 1:
	print 'At least 1 chair must be empty'
	help()
if steps < 1:
	print 'At least 1 step shall be used for defining goal state'
	help()

# hey, everything is ok. Let's start to do something.

##initialising things and putting together the PDDL code

goal_state=[]

print "(define (problem P"+str(people)+"E"+str(empty)+"S"+str(steps)+")"
print "  (:domain ChairGame)\n(:objects"

i=1
while i < people+1:
	goal_state.append("P"+str(i))
	print " P"+str(i),
	i=i+1

print " - person"

i=1
while i < empty+1:
	goal_state.append("NO"+str(i))
	print " NO"+str(i),
	i=i+1

print " - empty )"


initial_state=list(goal_state)


j = 0
while j < steps:
	x=random.randint(1,empty)
	to_search="NO"+str(x)
	position=initial_state.index(to_search)
	change=int(position)
	y=random.randint(0,1)
	if y == 0:
		change=change-3
		if change < 0:
			change=change+len(initial_state)
	else:
		change=change+3
		if change > len(initial_state)-1:
			change=change%len(initial_state)
	if not "NO" in initial_state[change]:
		temp=initial_state[position]
		initial_state[position]=initial_state[change]
		initial_state[change]=temp
	j=j+1

##print initial and goal states

print "(:init"
i=1
while i < len(initial_state):
	print "(before "+initial_state[i-1]+" "+initial_state[i]+")"
	i=i+1
print "(before "+initial_state[len(initial_state)-1]+" "+initial_state[0]+")\n)"

print "(:goal (and"

i=2
while i < people+1:
	print "(before P"+str(i-1)+" P"+str(i)+")" 
	i=i+1

print "))\n)"