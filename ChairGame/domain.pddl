(define (domain ChairGame)
(:requirements :typing)
(:types person empty - elements)


(:predicates (before ?p - elements ?c - elements)

  )

(:action swap-clockwise 
	 :parameters (?p1 - elements ?e1 - empty ?p2 - elements ?p3 - elements ?p4 - elements ?ptoswap - person)
	 :precondition (and 
	        (before ?p1 ?ptoswap)
	        (before ?ptoswap ?p2)
	        (before ?p2 ?p3)
	        (before ?p3 ?e1)
	        (before ?e1 ?p4)
	        )
	 :effect (and
		   (not (before ?p1 ?ptoswap))
		   (not (before ?ptoswap ?p2))
		   (not (before ?p3 ?e1))
		   (not (before ?e1 ?p4))
		   (before ?p3 ?ptoswap)
		   (before ?ptoswap ?p4)
		   (before ?p1 ?e1)
		   (before ?e1 ?p2)
		   ))



(:action swap-anticlockwise 
	 :parameters (?p1 - elements ?e1 - empty ?p2 - elements ?p3 - elements ?p4 - elements ?ptoswap - person)
	 :precondition (and 
	        (before ?p1 ?e1)
	        (before ?e1 ?p2)
	        (before ?p2 ?p3)
	        (before ?p3 ?ptoswap)
	        (before ?ptoswap ?p4)
	        )
	 :effect (and
		   (before ?p1 ?ptoswap)
		   (before ?ptoswap ?p2)
		   (before ?p3 ?e1)
		   (before ?e1 ?p4)
		    (not (before ?p3 ?ptoswap))
		    (not (before ?ptoswap ?p4))
		    (not (before ?p1 ?e1))
		    (not (before ?e1 ?p2))
		   ))

)