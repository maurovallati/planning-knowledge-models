Automatic music composition is a fascinating field within computational creativity.

Here we provide two different techniques that rely on automated planning for generating musical structures. The structures are then filled from the bottom
with �raw� musical materials, and turned into melodies.

Specifically, we design two techniques, i.e., flattening and tree, that are able to produce a top-down musical structure for melodies, based respectively on a one-level and a multi-level hierarchical
generative processes.

**Reference** This approach has been described in:
V. Velardo, M. Vallati, A Planning-based Approach for Music Composition, In Proceedings of the 11th International Symposium on Computer Music Multidisciplinary Research (CMMR-15), 2015.


Examples generated with the approach can be found here:
https://goo.gl/frRcVv