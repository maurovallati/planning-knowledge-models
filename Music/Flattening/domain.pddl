(define (domain music)
(:requirements :typing :equality :action-costs)
  (:types  
	museme
	)

  (:predicates
    (is_before ?x - museme ?y - museme) ;; indicates that the museme ?x comes before ?y, at any point
    (clear ?xy - museme) ;; it needs to be processed
    (processed ?xy - museme) ;; the museme has been processed
    (is_new ?x - museme) ;; a new museme, not a modification of something before
    (is_modification ?x ?y - museme) ;; modified verion of a museme that comes before
    (attached_to ?x ?y - museme) ;; indicates the museme that follows this one -- just for ordering purposes
  )

(:functions (total-cost))

(:action it_is_new
:parameters (?x - museme)
:precondition 
	(clear ?x)
:effect (and
	(processed ?x)
;	(is_new ?x)
	(increase (total-cost) 5)
	)
)

(:action it_modifies
:parameters (?x ?y - museme)
:precondition (and
	    (is_before ?y ?x)
	    (processed ?y)
	    (clear ?x)
	)
:effect (and
	(processed ?x)
	(increase (total-cost) 6)
)

)

(:action is_equal_to
:parameters (?x ?y - museme)
:precondition (and
	    (is_before ?y ?x)
	    (processed ?y)
	    (clear ?x)
	)
:effect (and
	(processed ?x)
	(increase (total-cost) 4)
)

)

)