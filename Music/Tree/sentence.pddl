(define (domain music)
(:requirements :typing :equality :action-costs)
  (:types  
	element
	)

  (:predicates
    (is_before ?x - element ?y - element) ;; indicates that the element ?x comes before ?y, at any point
    (clear ?xy - element) ;; it needs to be processed
    (processed ?xy - element) ;; the element has been processed
    (is_new ?x - element) ;; a new element, not a modification of something before
    (is_modification ?x ?y - element) ;; modified verion of a element that comes before
    (attached_to ?x ?y - element) ;; indicates the element that follows this one -- just for ordering purposes
  )

(:functions (total-cost))

(:action it_is_new
:parameters (?x - element)
:precondition 
	(clear ?x)
:effect (and
	(processed ?x)
;	(is_new ?x)
	(increase (total-cost) 1)
	)
)

(:action it_modifies
:parameters (?x ?y - element)
:precondition (and
	    (is_before ?y ?x)
	    (processed ?y)
	    (clear ?x)
	)
:effect (and
	(processed ?x)
	(increase (total-cost) 1)
)

)

(:action is_equal_to
:parameters (?x ?y - element)
:precondition (and
	    (is_before ?y ?x)
	    (processed ?y)
	    (clear ?x)
	)
:effect (and
	(processed ?x)
	(increase (total-cost) 50)
)

)

)