(define (domain music)
(:requirements :typing :equality :action-costs)
  (:types  
	sentence
	doubleph
	)

  (:predicates
    (is_before ?x - doubleph ?y -doubleph) ;; indicates that the element ?x comes before ?y, at any point
    (clear ?xy -doubleph) ;; it needs to be processed
    (clear_similar ?xy - doubleph ?z ?h - sentence) ;; it needs to be processed, but it is similar
    (clear_equal ?xy ?yz -doubleph) ;; it needs to be processed and forced to be similiar
    (processed ?xy - doubleph) ;; the element has been processed
    (is_new ?x - doubleph) ;; a new element, not a modification of something before
    (is_modification ?x ?y - doubleph) ;; modified verion of a element that comes before
    (attached_to ?x  - doubleph ?y - sentence) ;; indicates that the doubleph is a member of the sentence
    (different ?x ?y - doubleph)
  )

(:functions (total-cost))

(:action it_is_new
:parameters (?x - doubleph)
:precondition 
	(clear ?x)
:effect (and
	(processed ?x)
	(is_new ?x)
	(increase (total-cost) 5)
	)
)

(:action it_modifies
:parameters (?x ?y - doubleph)
:precondition (and
	    (is_before ?y ?x)
	    (processed ?y)
	    (clear ?x)
	)
:effect (and
	(processed ?x)
	(increase (total-cost) 2)
)
)

(:action is_equal_to
:parameters (?x ?y - doubleph)
:precondition (and
	    (is_before ?y ?x)
	    (processed ?y)
	    (clear ?x)
	)
:effect (and
	(processed ?x)
	(increase (total-cost) 5)
)
)


(:action forced_is_equal_to
:parameters (?x ?y - doubleph)
:precondition (and
	    (processed ?y)
		(clear_equal ?x ?y)
	)
:effect (and
	(processed ?x)
	(increase (total-cost) 5)
)
)



;; x diverso, y modificato

(:action process_similar_sec_Mod
:parameters (?x ?y ?z - doubleph ?primo ?secondo - sentence)
:precondition (and
		(clear_similar ?x ?primo ?secondo)
		(clear_similar ?y ?primo ?secondo)
	    (is_before ?z ?x)
		(different ?x ?y)
        (attached_to ?z ?primo)
        (attached_to ?x ?secondo)
        (attached_to ?y ?secondo)
	    (processed ?z)
	)
:effect (and
	(processed ?x)
	(processed ?y)
    (is_new ?x)
	(increase (total-cost) 5)
)
)

;; x diverso, y identico

(:action process_similar_sec_Eq
:parameters (?x ?y ?z - doubleph ?primo ?secondo - sentence)
:precondition (and
		(clear_similar ?x ?primo ?secondo)
		(clear_similar ?y ?primo ?secondo)
		(different ?x ?y)
	    (is_before ?z ?x)
        (attached_to ?z ?primo)
        (attached_to ?x ?secondo)
        (attached_to ?y ?secondo)
	    (processed ?z)
	)
:effect (and
	(processed ?x)
	(processed ?y)
    (is_new ?x)
	(increase (total-cost) 5)
)
)

)