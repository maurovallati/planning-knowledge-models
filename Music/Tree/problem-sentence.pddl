(define (problemsentence) 
 (:domain music) 
 (:objects
e1 e2 e3 e4  - element
)
(:init
(clear e1)
(clear e2)
(clear e3)
(clear e4)
(is_before e1 e2)
(is_before e1 e3)
(is_before e1 e4)
(is_before e2 e3)
(is_before e2 e4)
(is_before e3 e4)
)
(:goal
(and
(processed e1)
(processed e2)
(processed e3)
(processed e4)
))
(:metric minimize ( total-cost ))
)