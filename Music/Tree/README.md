Each pddl file represents one level of the hierarchy. 3 different problems have to be created and solved. 

1. Initially, a sentence problem is generated (the higher level in the musical hierarchy).
2. The solution from 1., is used to feed a doublephrase problem; 
3. Finally, the solution from 2. is provided to an appropriately designed museme problem, which represents the lower level.

