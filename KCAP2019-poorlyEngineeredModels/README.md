Here you can find the domain models used in the evaluation of the paper: 

Mauro Vallati, Lukas Chrpa:
On the Robustness of Domain-Independent Planning Engines: The Impact of Poorly-Engineered Knowledge. K-CAP 2019: 197-204

( https://dl.acm.org/citation.cfm?doid=3360901.3364416 )

The models are divided into 2 sets.

*-deadEndMinimal corresponds to the "minimal" version of the models injected with an opposing operator leading to dead ends. They can be downloaded from
https://bitbucket.org/maurovallati/planning-knowledge-models/downloads/minimally-modified-models.zip

*-mutex corresponds to the "minimal" version of the models injected with an opposing operator that has preconditions in mutex relationships. They can be found at:
https://bitbucket.org/maurovallati/planning-knowledge-models/downloads/modified-models.zip