Road Traffic Accident Management is a life-critical task that deals with effective planning of emergency response when accidents occur, in order to mitigate negative effects, especially saving human lives that might be in imminent danger. 

This model focuses on providing necessary treatment for victims injured during accidents. This involves coordination of medical teams responsible for providing medical treatment to the victims and fire brigades that are required to release victims trapped in damaged vehicles.


** Reference **
This model has been introduced in:
Lukás Chrpa, Mauro Vallati, On the exploitation of Automated Planning for efficient decision making in road traffic accident management. CDC 2016: 6607-6612
