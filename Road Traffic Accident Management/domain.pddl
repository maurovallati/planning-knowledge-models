(define (domain ambulance)
(:requirements :typing :durative-actions :fluents)
(:types 
	asset agent location - object
        mobile_asset static_asset - asset
	ambulance fire_brigade - need_driver
	medical_car medical_motorbike - no_driver
	need_driver no_driver - mobile_asset
        hospital fire_station rescue_garage building incidented_car - static_asset
    fireman-crew driver_agent medical_agent - agent
	paramedic emt - medical_agent
    critical-victim noncritical-victim - victim

)

(:predicates (connected ?l1 ?l2 - location)
             (compatible ?a - agent ?b - asset)
	         (at ?x - object ?l - location)
             (inside ?a - agent ?x - asset)
             (in ?v - victim ?x - ambulance)
             (onfire ?x - asset)
             (extinguished ?x - asset)
             (injured ?v - victim)
             (trapped ?v - victim ?x - asset)
             (firstaided ?v - victim)
             (hospitalised ?v - victim)
             (available ?a - agent)
             (free ?m - asset)
			 (notused ?m - mobile_asset)
)

(:functions (distance ?l1 ?l2 - location) 
            (speed ?e - mobile_asset) ;; speed will also consider a "traffic index"

)

;; GENERAL ACTIONS FOR: vehicles that need a specific driver, entering existing buildings and boarding  / debarking mobile assets.

(:durative-action move-driver
:parameters (?e - need_driver ?d - driver_agent ?l1 ?l2 - location)
:duration (= ?duration (/ (distance ?l1 ?l2) (speed ?e)))
:condition (and (over all (connected ?l1 ?l2)) (over all (inside ?d ?e)) (over all (compatible ?d ?e)) (at start (at ?e ?l1)))
:effect (and (at start (not (at ?e ?l1)))(at end (at ?e ?l2)))
)

(:durative-action move-no_driver
:parameters (?e - no_driver ?d - agent ?l1 ?l2 - location)
:duration (= ?duration (/ (distance ?l1 ?l2) (speed ?e)))
:condition (and (over all (connected ?l1 ?l2)) (over all (inside ?d ?e)) (over all (compatible ?d ?e)) (at start (at ?e ?l1)))
:effect (and (at start (not (at ?e ?l1)))(at end (at ?e ?l2)))
)


(:durative-action board
:parameters (?z - agent ?x - mobile_asset ?l - location)
:duration (= ?duration 1) ;to adjust..
:condition (and (at start (at ?z ?l))(over all (at ?x ?l))(over all (compatible ?z ?x)))
:effect (and (at start (not (at ?z ?l)))(at end (inside ?z ?x)))
)

(:durative-action debark
:parameters (?z - agent ?x - mobile_asset ?l - location)
:duration (= ?duration 1) ;to adjust..
:condition (and (at start (inside ?z ?x))(over all (at ?x ?l))(over all (compatible ?z ?x)))
:effect (and (at start (not (inside ?z ?x)))(at end (at ?z ?l)))
)

(:durative-action enter
:parameters (?z - agent ?x - static_asset ?l - location)
:duration (= ?duration 1) ;to adjust..
:condition (and (at start (at ?z ?l)) (at start (at ?x ?l) ))
:effect (and (at start (not (at ?z ?l)))(at end (inside ?z ?x)))
)

(:durative-action exit
:parameters (?z - agent ?x - static_asset ?l - location)
:duration (= ?duration 1) ;to adjust..
:condition (and (at start (inside ?z ?x)) (at start (at ?x ?l)))
:effect (and (at start (not (inside ?z ?x)))(at end (at ?z ?l)))
)

;; ACTIONS FOR FIRE CREW: EXTINGUISH FIRE AND UNTRAP VICTIMS FROM ACCIDENTED CARS

(:durative-action extinguish
:parameters (?f - fireman-crew ?fb - fire_brigade ?x - asset ?l -location)
:duration (= ?duration 20)
:condition (and (over all (at ?f ?l))(over all (at ?fb ?l))(at start (available ?f)) (at start (onfire ?x)) (at start (notused ?fb)) (over all (at ?x ?l)))
:effect (and (at start (not (available ?f)))(at end (available ?f))(at start (not (notused ?fb)))(at end (notused ?fb))(at end (not (onfire ?x)))(at end (extinguished ?x)))
)

(:durative-action untrap-victim
:parameters (?f - fireman-crew ?fb - fire_brigade ?x - asset ?v - victim ?l -location)
:duration (= ?duration 20)
:condition (and (over all (at ?f ?l))(over all (at ?fb ?l))(at start (available ?f)) (at start (notused ?fb))  (at start (trapped ?v ?x)) (over all (at ?x ?l)))
:effect (and (at start (not (available ?f)))(at end (available ?f))(at start (not (notused ?fb)))(at end (notused ?fb))(at end (not (trapped ?v ?x)))(at end (at ?v ?l)))
)

;; MEDICAL ACTIONS: FIRSTAID OF CRITICAL / NONCRITICAL, LOAD-UNLOAD VICTIMS ON AMBULANCES AND HOSPITALISE 

(:durative-action firstaid-noncritical
:parameters (?p - medical_agent  ?v - noncritical-victim ?l - location)
:duration (= ?duration 5)
:condition (and (over all (at ?p ?l))(at start (available ?p))(at start (injured ?v)) (over all (at ?v ?l)))
:effect (and (at start (not (available ?p)))(at end (available ?p))(at end (not (injured ?v)))(at end (firstaided ?v)))
)

(:durative-action firstaid-critical
:parameters (?p - paramedic  ?v - critical-victim ?l - location)
:duration (= ?duration 10)
:condition (and (over all (at ?p ?l))(at start (available ?p))(at start (injured ?v)) (over all (at ?v ?l)))
:effect (and (at start (not (available ?p)))(at end (available ?p))(at end (not (injured ?v)))(at end (firstaided ?v)))
)


(:durative-action load-victim
:parameters (?z - victim ?x - ambulance ?l - location ?p - medical_agent)
:duration (= ?duration 1) 
:condition (and (at start (at ?z ?l)) (at start (firstaided ?z))(at start (free ?x))(over all (at ?x ?l)) (over all (at ?p ?l)) (at start (available ?p)) (over all (compatible ?p ?x)) )
:effect (and (at start (not (available ?p)))(at end (available ?p))(at start (not (at ?z ?l)))(at end (in ?z ?x))(at end (not (free ?x))))
)

(:durative-action unload-victim
:parameters (?z - victim ?x - ambulance ?l - location ?p - medical_agent ?h - hospital )
:duration (= ?duration 4) 
:condition (and (over all (at ?p ?l)) (at start (available ?p)) (over all (compatible ?p ?x)) (over all (at ?x ?l)) (at start (in ?z ?x)) (at start (at ?h ?l) ))
:effect (and (at start (not (available ?p)))(at end (available ?p)) (at end (free ?x)) (at start (not (in ?z ?x))) (at end (hospitalised ?z)) )
)

;;(:durative-action hospitalise
;;:parameters (?z - medical_agent ?x - hospital ?l - location ?v - victim)
;;:duration (= ?duration 5) 
;;:condition (and (at start (at ?z ?l)) (at start (at ?x ?l) ) (at start (at ?v ?l)) (at start (available ?z)) )
;;:effect (and (at start (not (at ?z ?l))) (at start (not (available ?z)))(at end (available ?z)) (at start (not (at ?v ?l))) (at end (inside ?z ?x)) (at end (hospitalised ?v))  )
;;)

)