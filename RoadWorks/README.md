The road works domain model presented in:

Mauro Vallati, Lukas Chrpa and Diane Kitchin, How to Plan Roadworks in Urban Regions? A Principled Approach Based on AI Planning, ICCS 2019
