(define (domain works)

(:types roadwork -object road -object timeslot -object area - object company - object)


(:predicates (subsequent ?t - timeslot ?t2 - timeslot)
			 (tostart ?r -roadwork)
			 (onroad ?w -roadwork ?r - road)
			 (connected ?r -road ?r2 - road)
			(ongoingwork ?w - roadwork ?t - timeslot)
			 (inarea ?a - area ?r - road)
			 (assigned ?c - company ?w - roadwork)
			
)

(:functions 
	(parallelwork ?r - area ?t - timeslot) 
	(companywork ?c - company ?t - timeslot)
	;;(reduction ?w - roadwork) 
	(duration ?w - roadwork) (value ?t -timeslot) (deadline ?w - roadwork) (startime ?w - roadwork)
	(initiated)
	(total-cost)
)


;(:constraint simultaneous
;    :parameters (?a - area ?t - timeslot)
;    :condition (< (parallelwork ?a ?t) 2)
;)

;;(:constraint simultaneous
;;    :parameters (?c - company ?t - timeslot)
;;    :condition (<= (companywork ?a ?t) 2)
;;)


(:action first-assign
:parameters (?w - roadwork ?r - road ?t - timeslot ?a - area ?c - company) 
:precondition (and 
		(tostart ?w)
		(inarea ?a ?r)
		(onroad ?w ?r)
		(assigned ?c ?w)
		(<= (+ (value ?t) (duration ?w)) (deadline ?w))
		(>= (value ?t) (startime ?w))
		(<= (parallelwork ?a ?t) 3)
	    )
:effect (and 
			(increase (parallelwork ?a ?t) 1 )
			(increase (companywork ?c ?t) 1)
			(not (tostart ?w))
			(ongoingwork ?w ?t)
			(decrease (duration ?w) 1)
			(increase (initiated) (value ?t))
			(increase (total-cost) (value ?t))
		)
)


(:action subsequent-assing
:parameters (?w - roadwork ?r - road ?t - timeslot ?t2 - timeslot ?a - area ?c - company) 
:precondition (and 
		(not (tostart ?w))
		(ongoingwork ?w ?t)
		(not (ongoingwork ?w ?t2))
		(assigned ?c ?w)
		(= (value ?t2) (+ (value ?t) 1))
		(inarea ?a ?r)
		(onroad ?w ?r)
		(<= (parallelwork ?a ?t) 3)
	    )
:effect (and 
			(increase (companywork ?c ?t) 1)
			(increase (parallelwork ?a ?t2) 1 )
			(ongoingwork ?w ?t2)
			(decrease (duration ?w) 1)
		)
)

)
