# Planning Knowledge Models

This repository includes the domain models (and possibly, generators) I developed (or co-developed) over time for competitions or for benchmarking planning engines.

Admittedly, I did not spend much time on organising the structure of the repository... but I'm happy to answer questions if something is unclear.

Relevant links:

# International Planning Competition, determistic track, 2014
https://helios.hud.ac.uk/scommv/IPC-14/

# International Planning Competition, Unsolvability track, 2016
http://unsolve-ipc.eng.unimelb.edu.au

# Sparkle Planning Challenge 2019
https://ada.liacs.nl/events/sparkle-planning-19/

# Planning for manipulating articulated objects
https://github.com/EmaroLab/paco_synthetic_test
- and the paper to cite is: 
	Alessio Capitanelli, Marco Maratea, Fulvio Mastrogiovanni, Mauro Vallati, 
	Automated Planning Techniques for Robot Manipulation Tasks Involving Articulated Objects. AI*IA 2017: 483-497

# Scenarios used in the International Competition on Knowledge Engineering for Planning and Scheduling (ICKEPS) 2016 
https://ickeps2016.wordpress.com/scenarios/
(we don't have encodings of those scenarios, but feel free to come up with your own)
